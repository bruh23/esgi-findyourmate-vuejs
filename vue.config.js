module.exports = {
  lintOnSave: false,

  devServer: {
    disableHostCheck: true,
  },

  transpileDependencies: ["vuetify"],

  runtimeCompiler: true,
};
