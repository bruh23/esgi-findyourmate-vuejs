import VueJwtDecode from "vue-jwt-decode";

export default function admin({ next, router }) {
  if (
    !JSON.parse(localStorage.getItem("vuetify@user")) &&
    !JSON.parse(localStorage.getItem("vuetify@user")).accessToken
  ) {
    console.log("token expiré");

    return router.push({
      name: "Login",
      params: {
        error_message: "Votre token a expiré veuillez vous reconnectez",
      },
    });
  } else if (
    JSON.parse(
      VueJwtDecode.decode(
        JSON.parse(localStorage.getItem("vuetify@user")).accessToken
      ) &&
        !JSON.parse(
          VueJwtDecode.decode(
            JSON.parse(localStorage.getItem("vuetify@user")).accessToken
          ).roles.includes("ROLE_ADMIN")
        )
    )
  ) {
    console.log(
      "ici",
      VueJwtDecode.decode(
        JSON.parse(localStorage.getItem("vuetify@user")).accessToken
      ).roles.includes("ROLE_ADMIN")
    );
    return router.push({
      name: "Evenement",
      params: { error_message: "Vous n'avez pas accès a cette page!" },
    });
  }

  return next();
}
