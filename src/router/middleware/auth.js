export default function auth({ next, router }) {
  if (
    JSON.parse(localStorage.getItem("vuetify@user")) &&
    !JSON.parse(localStorage.getItem("vuetify@user")).accessToken
  ) {
    console.log("token expiré");
    return router.push({
      name: "Login",
      params: {
        error_message: "Votre token a expiré veuillez vous reconnectez",
      },
    });
  }

  return next();
}
