// Imports
import { layout, route } from "@/util/routes";
import Vue from "vue";
import Router from "vue-router";
import admin from "./middleware/admin";
import auth from "./middleware/auth";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior: (to, from, savedPosition) => {
    if (to.hash) return { selector: to.hash };
    if (savedPosition) return savedPosition;

    return { x: 0, y: 0 };
  },
  routes: [
    layout("Default", [
      //Evenement
      route("Evenement", null, "/", {
        middleware: auth, //auth
      }),

      //Evenement Profil
      route("EvenementProfil", null, "evenements/:id", {
        middleware: auth, //auth
      }),

      //Evenement Liste
      route("Evenement Liste", null, "history", {
        middleware: auth, //auth
      }),

      //Evenement Creation
      route("Evenement Creation", null, "create/event", {
        middleware: auth, //auth
      }),

      //Profil
      route("Profil", null, "profil", {
        middleware: auth, //auth
      }),

      // TestVueMik
      route("TestVueMik", null, "components/testvuemik", {
        middleware: auth, //auth
      }),
    ]),
    layout("Admin", [
      route("Admin", null, "admin/dashboard", {
        middleware: admin,
      }),
      route("Admin Users", null, "admin/users", {
        middleware: admin,
      }),
      route("Admin Certifications", null, "admin/certifications", {
        middleware: admin,
      }),
      route("Admin Evenements", null, "admin/evenements", {
        middleware: admin,
      }),
      route("Admin Category", null, "admin/category", {
        middleware: admin,
      }),
    ]),

    layout("Auth", [
      // Login
      route("Login", null, "login"),

      // Sign up
      route("SignUp", null, "sign-up"),

      // ForgotPassword
      route("ForgotPassword", null, "forgot-password"),
    ]),
  ],
});

// Creates a `nextMiddleware()` function which not only
// runs the default `next()` callback but also triggers
// the subsequent Middleware function.
function nextFactory(context, middleware, index) {
  const subsequentMiddleware = middleware[index];
  // If no subsequent Middleware exists,
  // the default `next()` callback is returned.
  if (!subsequentMiddleware) return context.next;

  return (...parameters) => {
    // Run the default Vue Router `next()` callback first.
    context.next(...parameters);
    // Then run the subsequent Middleware with a new
    // `nextMiddleware()` callback.
    const nextMiddleware = nextFactory(context, middleware, index + 1);
    subsequentMiddleware({ ...context, next: nextMiddleware });
  };
}

router.beforeEach((to, from, next) => {
  if (to.meta.middleware) {
    const middleware = Array.isArray(to.meta.middleware)
      ? to.meta.middleware
      : [to.meta.middleware];

    const context = {
      from,
      next,
      router,
      to,
    };
    const nextMiddleware = nextFactory(context, middleware, 1);

    return middleware[0]({ ...context, next: nextMiddleware });
  }

  return next();
});

// router.beforeEach((to, from, next) => {
//   return to.path.endsWith("/") ? next() : next(trailingSlash(to.path));
// });

export default router;
