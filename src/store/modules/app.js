// Pathify
import { make } from "vuex-pathify";
import Global from "../../api/Global";
// Data
const state = {
  drawer: null,
  drawerImage: true,
  mini: false,
  items: [
    {
      title: "Evenement",
      icon: "mdi-airballoon",
      to: "/",
    },
    {
      title: "Historique",
      icon: "mdi-airballoon",
      to: "/history/",
    },
    {
      title: "Profil",
      icon: "mdi-airballoon",
      to: "/profil/",
    },
    {
      title: "Créer un évenement",
      icon: "mdi-airballoon",
      to: "/create/event",
    },
  ],
  admin_items: [
    {
      title: "Dashboard",
      icon: "mdi-view-dashboard",
      to: "/admin/dashboard",
    },
    {
      title: "Utilisateurs",
      icon: "mdi-account",
      to: "/admin/users",
    },
    {
      title: "Certifications",
      icon: "mdi-check-circle",
      to: "/admin/certifications",
    },

    {
      title: "Evenements",
      icon: "mdi-calendar",
      to: "/admin/evenements",
    },
    {
      title: "Catégorie",
      icon: "mdi-shape",
      to: "/admin/category",
    },
  ],
  currentApi: new Global().fymApiPlatform,
};

const mutations = make.mutations(state);

const actions = {
  ...make.actions(state),
  init: async ({ dispatch }) => {
    //
  },
};

const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
