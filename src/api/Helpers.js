import VueJwtDecode from "vue-jwt-decode";
import api from ".";
import router from "../router";
import store from "../store";

export default class Helpers {
  constructor(Global) {
    this.global = new Global();
  }

  handleErrors(response) {
    return new Promise((resolve, reject) => {
      var contentType = response.headers.get("content-type");

      if (contentType && contentType.indexOf("application/json") !== -1) {
        resolve(response.json());
      } else {
        resolve({});
      }
    }).then((value) => {
      if (response.ok) return value;
      else {
        if (response.status === 401 && value.message === "Expired JWT Token") {
          this.tokenHaveExpired(this.global.refreshToken);
        }
        throw value;
      }
    });
  }

  tokenHaveExpired(token) {
    api
      .fym()
      .auth.refreshToken(token)
      .then((response) => {
        store.set("user/accessToken", response.token);
        store.set("user/decodedToken", VueJwtDecode.decode(response.token));
        store.set("user/refreshToken", response.refresh_token);
        router.history.push("/");
      })
      .catch((error) => {
        console.error("Error on refreshing token", error);
        router.history.push({
          name: "Login",
          params: {
            error_message: "Votre token a expiré veuillez vous reconnectez",
          },
        });
      });
  }

  createInsecureInstance() {
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    //Todo add other options...
    return {
      baseUrl: this.global.currentApi,
      options: {
        headers: myHeaders,
        redirect: "follow",
      },
    };
  }

  createSecureInstance() {
    let myHeaders = new Headers();

    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Accept", "application/json");
    myHeaders.append("Authorization", `Bearer ${this.global.accessToken}`);

    return {
      baseUrl: this.global.currentApi,
      options: {
        headers: myHeaders,
        redirect: "follow",
      },
    };
  }

  specificHeader(options) {
    if (this.global.currentApi === String(process.env.VUE_APP_API_PLATFORM)) {
      options.headers.set("Content-type", "application/merge-patch+json");
      return options;
    } else return options;
  }
}
