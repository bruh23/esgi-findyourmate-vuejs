import API from "./API";
import Global from "./Global";

const api = new API(Global);

export default api;
