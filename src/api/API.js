import FymApi from "./FYM/FymApi";
import Helpers from "./Helpers";

export default class API extends Helpers{
  constructor(Global) {
    super(Global);

    this.fym = () => new FymApi(Global);
  }
}
