import Helpers from "../Helpers";

export default class Auth extends Helpers {
  constructor(Global) {
    super(Global);

    let instance = this.createSecureInstance();

    this.options = instance.options;
    this.baseUrl = instance.baseUrl;
  }

  async createUser(email, password) {
    this.options.method = "POST";
    this.options.body = JSON.stringify({ email, password });

    return await fetch(`${this.baseUrl}users`, this.options).then((response) =>
      this.handleErrors(response)
    );
  }

  async getUsers() {
    this.options.method = "GET";

    return await fetch(`${this.baseUrl}users`, this.options).then((response) =>
      this.handleErrors(response)
    );
  }

  async editUser(id, data) {
    this.options.method = "PATCH";
    this.options.body = JSON.stringify(data);
    this.options = this.specificHeader(this.options);

    return await fetch(
      `${this.baseUrl}users/${id}`,
      this.options
    ).then((response) => this.handleErrors(response));
  }

  async deleteUser(id) {
    this.options.method = "DELETE";

    return await fetch(
      `${this.baseUrl}users/${id}`,
      this.options
    ).then((response) => this.handleErrors(response));
  }

  async getUser(id) {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}users/${id}`,
      this.options
    ).then((response) => this.handleErrors(response));
  }
}
