import Helpers from "../Helpers";

export default class Event extends Helpers {
  constructor(Global) {
    super(Global);

    let instance = this.createSecureInstance();

    this.options = instance.options;
    this.baseUrl = instance.baseUrl;
  }

  async getAllChat() {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}chats`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }

  async getChat(id) {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}chats/${id}`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }
}
