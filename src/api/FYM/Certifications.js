import Helpers from "../Helpers";

export default class Certifications extends Helpers {
  constructor(Global) {
    super(Global);

    let instance = this.createSecureInstance();

    this.options = instance.options;
    this.baseUrl = instance.baseUrl;
  }

  async getCertifications() {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}certifications`,
      this.options
    ).then((response) => this.handleErrors(response));
  }

  async createCertification(data) {
    this.options.method = "POST";
    this.options.body = JSON.stringify(data);

    return await fetch(
      `${this.baseUrl}certifications`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }

  async getCertification(id) {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}certifications/${id}`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }

  async updateCertification(id, data) {
    this.options.method = "PATCH";
    this.options.body = JSON.stringify(data);
    this.options = this.specificHeader(this.options);

    return await fetch(
      `${this.baseUrl}certifications/${id}`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }
}
