import Helpers from "../Helpers";

export default class Event extends Helpers {
  constructor(Global) {
    super(Global);

    let instance = this.createSecureInstance();

    this.options = instance.options;
    this.baseUrl = instance.baseUrl;
  }

  async getAllEventRegistration() {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}event_registrations`,
      this.options
    ).then((response) => this.handleErrors(response));
  }

  async createEventRegistration(data) {
    this.options.method = "POST";
    this.options.body = JSON.stringify(data);

    return await fetch(
      `${this.baseUrl}event_registrations`,
      this.options
    ).then((response) => this.handleErrors(response));
  }

  async getEventRegistration(id) {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}event_registrations/${id}`,
      this.options
    ).then((response) => this.handleErrors(response));
  }

  async deleteEventRegistration(id) {
    this.options.method = "DELETE";

    return await fetch(
      `${this.baseUrl}event_registrations/${id}`,
      this.options
    ).then((response) => this.handleErrors(response));
  }

  async updateEventRegistration(id, data) {
    this.options.method = "PATCH";
    this.options.body = JSON.stringify(data);
    this.options = this.specificHeader(this.options);

    return await fetch(
      `${this.baseUrl}event_registrations/${id}`,
      this.options
    ).then((response) => this.handleErrors(response));
  }
}
