import Helpers from "../Helpers";

export default class Event extends Helpers {
  constructor(Global) {
    super(Global);

    let instance = this.createSecureInstance();

    this.options = instance.options;
    this.baseUrl = instance.baseUrl;
  }

  async getAllEvaluation() {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}evaluations`,
      this.options
    ).then((response) => this.handleErrors(response));
  }

  async createEvaluation(data) {
    this.options.method = "POST";
    this.options.body = JSON.stringify(data);

    return await fetch(
      `${this.baseUrl}evaluations`,
      this.options
    ).then((response) => this.handleErrors(response));
  }

  async getEvaluation(id) {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}evaluations/${id}`,
      this.options
    ).then((response) => this.handleErrors(response));
  }

  async deleteEvaluation(id) {
    this.options.method = "DELETE";

    return await fetch(
      `${this.baseUrl}evaluations/${id}`,
      this.options
    ).then((response) => this.handleErrors(response));
  }

  async updateEvaluation(id, data) {
    this.options.method = "PATCH";
    this.options.body = JSON.stringify(data);
    this.options = this.specificHeader(this.options);

    return await fetch(
      `${this.baseUrl}evaluations/${id}`,
      this.options
    ).then((response) => this.handleErrors(response));
  }
}
