import Helpers from "../Helpers";

export default class Event extends Helpers {
  constructor(Global) {
    super(Global);

    let instance = this.createSecureInstance();

    this.options = instance.options;
    this.baseUrl = instance.baseUrl;
  }

  async getAllSignalement() {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}signalements`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }

  async createSignalement(data) {
    this.options.method = "POST";
    this.options.body = JSON.stringify({data});

    return await fetch(
      `${this.baseUrl}signalements`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }

  async getSignalement(id) {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}signalements/${id}`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }
}
