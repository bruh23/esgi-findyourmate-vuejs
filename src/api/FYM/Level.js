import Helpers from "../Helpers";

export default class Event extends Helpers {
  constructor(Global) {
    super(Global);

    let instance = this.createSecureInstance();

    this.options = instance.options;
    this.baseUrl = instance.baseUrl;
  }

  async getAllLevel() {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}levels`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }

  async getLevel(id) {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}levels/${id}`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }
}
