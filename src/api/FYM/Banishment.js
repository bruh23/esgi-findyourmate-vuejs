import Helpers from "../Helpers";

export default class Event extends Helpers {
  constructor(Global) {
    super(Global);

    let instance = this.createSecureInstance();

    this.options = instance.options;
    this.baseUrl = instance.baseUrl;
  }

  async getAllBanishment() {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}banishment`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }

  async getBanishment(id) {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}banishment/${id}`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }
}
