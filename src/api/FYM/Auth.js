import Helpers from "../Helpers";

export default class Auth extends Helpers {
  constructor(Global) {
    super(Global);

    let instance = this.createInsecureInstance();

    this.options = instance.options;
    this.baseUrl = instance.baseUrl;
  }

  async login(email, password) {
    this.options.method = "POST";
    this.options.body = JSON.stringify({ email, password });

    return await fetch(
      `${this.baseUrl}find_authentication_token`,
      this.options
    ).then((response) => this.handleErrors(response));
  }

  async signup(data) {
    this.options.method = "POST";

    // Acctually we nonly need email and password
    this.options.body = JSON.stringify({
      email: data.email,
      password: data.password,
    });

    return await fetch(`${this.baseUrl}users`, this.options).then((response) =>
      this.handleErrors(response)
    );
  }

  async remindpassword(email) {
    this.options.method = "POST";
    this.options.body = JSON.stringify({ email });

    return await fetch(
      `${this.baseUrl}reset-password/request`,
      this.options
    ).then((response) => this.handleErrors(response));
  }

  async refreshToken(refresh_token) {
    //confirm with raouf
    this.options.method = "POST";
    this.options.body = JSON.stringify({ refresh_token });

    return await fetch(
      `${this.baseUrl}token/refresh`,
      this.options
    ).then((response) => this.handleErrors(response));
  }
}
