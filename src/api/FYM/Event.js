import Helpers from "../Helpers";

export default class Event extends Helpers {
  constructor(Global) {
    super(Global);

    let instance = this.createSecureInstance();

    this.options = instance.options;
    this.baseUrl = instance.baseUrl;
  }

  async getAllEvent(params) {
    this.options.method = "GET";

    let url = new URL(`${this.baseUrl}events`);
    url.search = new URLSearchParams(params);

    return await fetch(url, this.options).then((response) =>
      this.handleErrors(response)
    );
  }

  async createEvent(data) {
    this.options.method = "POST";
    data.category = `categories/${data.category}`;
    this.options.body = JSON.stringify(data);

    return await fetch(`${this.baseUrl}events`, this.options).then((response) =>
      this.handleErrors(response)
    );
  }

  async getEvent(id) {
    console.log(id);
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}events/${id}`,
      this.options
    ).then((response) => this.handleErrors(response));
  }

  async deleteEvent(id) {
    this.options.method = "DELETE";

    return await fetch(
      `${this.baseUrl}events/${id}`,
      this.options
    ).then((response) => this.handleErrors(response));
  }

  async updateEvent(id, data) {
    this.options.method = "PATCH";
    data.category = `categories/${data.category}`;

    this.options.body = JSON.stringify(data);
    this.options = this.specificHeader(this.options);

    return await fetch(
      `${this.baseUrl}events/${id}`,
      this.options
    ).then((response) => this.handleErrors(response));
  }
}
