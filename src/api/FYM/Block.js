import Helpers from "../Helpers";

export default class Event extends Helpers {
  constructor(Global) {
    super(Global);

    let instance = this.createSecureInstance();

    this.options = instance.options;
    this.baseUrl = instance.baseUrl;
  }

  async getAllBlock() {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}blocks`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }

  async createBlock(data) {
    this.options.method = "POST";
    this.options.body = JSON.stringify({data});

    return await fetch(
      `${this.baseUrl}blocks`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }

  async getBlock(id) {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}blocks/${id}`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }
}
