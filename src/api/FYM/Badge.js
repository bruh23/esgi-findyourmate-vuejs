import Helpers from "../Helpers";

export default class Event extends Helpers {
  constructor(Global) {
    super(Global);

    let instance = this.createSecureInstance();

    this.options = instance.options;
    this.baseUrl = instance.baseUrl;
  }

  async getAllBadge() {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}badges`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }

  async getBadge(id) {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}badges/${id}`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }
}
