import Helpers from "../Helpers";
import Auth from "./Auth";
import Badge from "./Badge";
import Banishment from "./Banishment";
import Block from "./Block";
import Category from "./Category";
import Certifications from "./Certifications";
import Chat from "./Chat";
import Evaluation from "./Evaluation";
import Event from "./Event";
import EventRegistration from "./EventRegistration";
import Level from "./Level";
import Signalement from "./Signalement";
import User from "./User";

export default class FymApi extends Helpers {
  constructor(Global) {
    super(Global);

    this.client = {};

    this.auth = new Auth(Global);
    this.badge = new Badge(Global);
    this.banishment = new Banishment(Global);
    this.block = new Block(Global);
    this.category = new Category(Global);
    this.certfications = new Certifications(Global);
    this.chat = new Chat(Global);
    this.evaluation = new Evaluation(Global);
    this.event = new Event(Global);
    this.eventRegistration = new EventRegistration(Global);
    this.level = new Level(Global);
    this.signalement = new Signalement(Global);
    this.user = new User(Global);
  }
}
