import Helpers from "../Helpers";

export default class Event extends Helpers {
  constructor(Global) {
    super(Global);

    let instance = this.createSecureInstance();

    this.options = instance.options;
    this.baseUrl = instance.baseUrl;
  }

  async getAllCategory() {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}categories`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }

  async getCategory(id) {
    this.options.method = "GET";

    return await fetch(
      `${this.baseUrl}categories/${id}`,
      this.options
    ).then((response) => this.handleErrors(response)); 
  }
}
