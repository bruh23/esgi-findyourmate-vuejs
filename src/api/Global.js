import store from "../store";

export default class Global {
  get accessToken() {
    if (
      JSON.parse(localStorage.getItem("vuetify@user")) &&
      JSON.parse(localStorage.getItem("vuetify@user")).accessToken
    )
      return JSON.parse(localStorage.getItem("vuetify@user")).accessToken;
    else return "";
  }

  get refreshToken() {
    return store.get("user/refreshToken");
  }

  set accessToken(accessToken) {
    // return setGlobal({ accessToken: accessToken });
  }

  get user() {
    // return getGlobal().user;
    return store.get("user");
  }

  get fymApiPlatform() {
    return String(process.env.VUE_APP_API_PLATFORM);
  }

  get fymPrisma() {
    return String(process.env.VUE_APP_API_PRISMA);
  }

  get currentApi() {
    return store.get("app/currentApi");
  }
}
